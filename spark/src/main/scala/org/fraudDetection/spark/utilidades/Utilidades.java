package org.fraudDetection.spark.utilidades;

import org.fraudDetection.spark.configuracion.ConfiguracionAlgoritmoDeteccion;

public class Utilidades {
	
	private static Utilidades instancia;
	
	public  static void crearInstancia(){
		if(instancia==null)instancia=new Utilidades();
	}
	
	public static Utilidades getIntsancia(){
		return instancia;
	}
	
	public Utilidades(){
		ConfiguracionAlgoritmoDeteccion.crearInstancia();
	}
	
	public double ajustarValor(Object input){
		if(input == null)return 0.5;//En caso de no existir algun atributo del map
		else{
			if(input instanceof String) return Double.parseDouble((String)input);
			else return (Double)input;			
		}
	}
	
	public boolean existeAtributoCategorico(String input){		
		return ConfiguracionAlgoritmoDeteccion.getIntsancia().getAtributosCategoricos().contains(input);
	}
	
	public boolean existeAtributoNoCategorico(String input){
		return ConfiguracionAlgoritmoDeteccion.getIntsancia().getAtributosNoCategoricos().contains(input);
	}
	
	public double distribucionGausiana(double cantidad, double media, double varianza){
		double pi=3.14;
		double cantidadAjustada= Math.log(cantidad);
	    if(varianza==0)varianza=0.0000001; //si es 0 falla
		double resulDenomBase=Math.sqrt((2*pi*varianza));
		double base=1/resulDenomBase;
		double numeradorExp=((cantidadAjustada-media)*(cantidadAjustada-media));
		double denominadorExp=2*varianza;
		double exp=-(numeradorExp/denominadorExp);
		//base=Math.roundFraction(base,2);
		//exp=Math.roundFraction(exp,2);
		if(base!=0){
			double resultado=Math.exp(exp);
			resultado=resultado*base;
			return resultado;
		}
		else return 0;
	}

}
