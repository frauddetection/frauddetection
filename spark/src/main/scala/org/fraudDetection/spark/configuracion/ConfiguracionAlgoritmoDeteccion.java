package org.fraudDetection.spark.configuracion;

import java.util.ArrayList;

public class ConfiguracionAlgoritmoDeteccion {
	
	private static ConfiguracionAlgoritmoDeteccion instancia;
	
	//Anomaly
	private double epsilon;
	private int movimientosMinimos;
	//DB
	private String dirDB;
	private int puertoDB;
	private String nombreDB;
	private String coleccion;
	//Atributos del sistema
	private ArrayList<String> atributosCategoricos;
	private ArrayList<String> atributosNoCategoricos;
	//Producer
	private String topic;
	private int eventos;
	private int tiempoPorcesamiento; //en segundos
	
	public  static void crearInstancia(){
		if(instancia==null)instancia=new ConfiguracionAlgoritmoDeteccion();
	}
	
	public static ConfiguracionAlgoritmoDeteccion getIntsancia(){
		return instancia;
	}
	
	public ConfiguracionAlgoritmoDeteccion(){
		this.epsilon = 0.001;
		this.movimientosMinimos = 35;
		this.topic = "test";
		this.eventos = 500;
		this.tiempoPorcesamiento = 5;
		this.dirDB = "localhost";
		this.puertoDB = 27017;
		this.nombreDB = "MiDB";
		this.coleccion = "Pruebas";
		this.atributosCategoricos = new ArrayList<String>();
		this.atributosNoCategoricos = new ArrayList<String>();
		initAtributosCategoricos();
		initAtributosNoCategoricos();
	}
	
	private void initAtributosCategoricos(){
		atributosCategoricos.add("Rama");
		atributosCategoricos.add("Lugar");
		atributosCategoricos.add("Modo");
	}
	
	private void initAtributosNoCategoricos(){
		atributosNoCategoricos.add("Cantidad");
	}
	
	public ArrayList<String> getAtributosCategoricos(){
		return this.atributosCategoricos;
	}
	
	public ArrayList<String> getAtributosNoCategoricos(){
		return this.atributosNoCategoricos;
	}
	
	public double getEpsilon() {
		return epsilon;
	}

	public int getMovimientosMinimos() {
		return movimientosMinimos;
	}

	public String getTopic() {
		return topic;
	}

	public int getEventos() {
		return eventos;
	}

	public int getTiempoPorcesamiento() {
		return tiempoPorcesamiento;
	}

	public String getDirDB() {
		return dirDB;
	}

	public int getPuertoDB() {
		return puertoDB;
	}

	public String getNombreDB() {
		return nombreDB;
	}


	public String getColeccion() {
		return coleccion;
	}


	
}
