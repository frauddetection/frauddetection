package org.fraudDetection.spark

import kafka.producer.ProducerConfig

import java.util.Properties

import kafka.producer.Producer;
import scala.util.Random
import kafka.producer.KeyedMessage
import org.fraudDetection.spark.algoritmos.AlgoritmoGeneradorMensajes;

import java.util.Date

import org.fraudDetection.spark.configuracion.ConfiguracionAlgoritmoDeteccion;

object Producer extends App {
  val generadorMensajes = new AlgoritmoGeneradorMensajes();
  ConfiguracionAlgoritmoDeteccion.crearInstancia();
  val eventos = ConfiguracionAlgoritmoDeteccion.getIntsancia.getEventos();
  val topic = ConfiguracionAlgoritmoDeteccion.getIntsancia.getTopic();
  val brokers = "localhost:9092"
  val rnd = new Random()
  val props = new Properties()
  props.put("metadata.broker.list", brokers)
  props.put("serializer.class", "kafka.serializer.StringEncoder")
  //props.put("partitioner.class", "kafka.producer.Partitioner")
  props.put("producer.type", "async")
  props.put("request.required.acks", "1")

  val config = new ProducerConfig(props)
  val producer = new Producer[String, String](config)
  val t = System.currentTimeMillis()
  for (nEvents <- Range(0,eventos)) {
    val runtime = new Date().getTime();
    val key = "0";

    val Cantidad="10";
    val Nombre="Javier Mansilla";
    val Caducidad="19-18";
    val CVV="123";
    //NumTarjeta="1234123131233"; //Numero malo
    val NumTarjeta="4181583900000140";  //Numero correcto
    val Modo="on-line";
    val Rama="Alguna";
    val Lugar="Polonia";
    
    val msg = generadorMensajes.generaMensajeComportamientoExtraño();
    
    val data = new KeyedMessage[String, String](topic,key, msg);
    producer.send(data);
    System.out.println("Sending... " );
  }
  producer.close();
}