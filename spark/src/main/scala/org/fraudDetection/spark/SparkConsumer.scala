package org.fraudDetection.spark

import org.apache.spark.SparkConf
import org.apache.spark.streaming.StreamingContext;
import org.apache.spark.streaming.Seconds;
import org.apache.spark.streaming.Minutes;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.fraudDetection.spark.algoritmos.AlgoritmoLuhn;
import org.fraudDetection.spark.algoritmos.DeteccionAnomalias;
import org.fraudDetection.spark.configuracion.ConfiguracionAlgoritmoDeteccion;
import org.fraudDetection.spark.dataBase.Gestor_MongoDB;
import org.fraudDetection.spark.utilidades.Utilidades;


object SparkConsumer extends App {
    ConfiguracionAlgoritmoDeteccion.crearInstancia();
    Utilidades.crearInstancia();
    Gestor_MongoDB.crearInstancia();
    val zkQuorum = "localhost:2181";
    val topic = ConfiguracionAlgoritmoDeteccion.getIntsancia.getTopic();
    val tiempoProcesamiento = ConfiguracionAlgoritmoDeteccion.getIntsancia.getTiempoPorcesamiento();
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("SparkConsumer")
    val ssc = new StreamingContext(sparkConf, Seconds(tiempoProcesamiento))

    Gestor_MongoDB.getIntsancia.dbConnect();
    val testLuhn = new AlgoritmoLuhn();
    val anomalyDetection = new DeteccionAnomalias();
    
    
    val kafkaStream = KafkaUtils.createStream(ssc, zkQuorum, "consumer", Map(topic ->5))
    var lines = kafkaStream

    lines.foreachRDD(rdd => {
      for(item <- rdd.collect().toArray){
        var itemString = item.toString()
        var numTarjeta = "";
        itemString = itemString.substring(3,itemString.length()-1)
        var subStrings = itemString.split(";")
        for(atributo <- subStrings){
          var nombre_valor = atributo.split("=");
          var nombre = nombre_valor.apply(0);
          nombre = nombre.replaceAll(" ", "");
          var valor = nombre_valor.apply(1);
          valor.replaceAll(" ", "");
          if( Utilidades.getIntsancia.existeAtributoNoCategorico(nombre) || Utilidades.getIntsancia.existeAtributoCategorico(nombre) ){
            Gestor_MongoDB.getIntsancia.putAtributo(nombre,valor);
          }
          else if(nombre.equalsIgnoreCase("NumTarjeta")){
            numTarjeta = valor;
          }
        }
        Gestor_MongoDB.getIntsancia.putMovimiento("NumTarjeta", numTarjeta);
        var eventoActual = Gestor_MongoDB.getIntsancia.getStringEvent();
        if(testLuhn.testLuhn(numTarjeta)) { //Test de luhn ok
         
          if(Gestor_MongoDB.getIntsancia.consultaNumMovimientos(numTarjeta) <= ConfiguracionAlgoritmoDeteccion.getIntsancia.getMovimientosMinimos()){//No hay datos suficientes
            println(">>>>Insertando datos>>>>"); 
            Gestor_MongoDB.getIntsancia.insertar(numTarjeta);            
          }
          else{//Hay datos suficientes y aplicamos anomaly
            println(">>>>Aplicando anomaly detection>>>>");
            anomalyDetection.precalcular(numTarjeta);
            var deteccion = anomalyDetection.aplicarDeteccionAnomalias(eventoActual, Gestor_MongoDB.getIntsancia.consultaNumMovimientos(numTarjeta) ,ConfiguracionAlgoritmoDeteccion.getIntsancia.getEpsilon());
            if(deteccion ==true){
              println(">>>>Fraude detectado>>>>");
            }
            else{//Instertar en BD porque no es un fraude
              Gestor_MongoDB.getIntsancia.insertar(numTarjeta);
            }
          }
        }
        Gestor_MongoDB.getIntsancia.clearDocuments();//En TIBCO no hacía falta, aqui debemos limpiar el nuevo documento para qe no inserte el mismo siemre
      }
     })

    kafkaStream.print();
    ssc.start()
    ssc.awaitTermination()
}