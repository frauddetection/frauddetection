package org.fraudDetection.spark.dataBase;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.fraudDetection.spark.configuracion.ConfiguracionAlgoritmoDeteccion;

import com.mongodb.*;

public class Gestor_MongoDB {
	
	private static Gestor_MongoDB instancia;
	
	BasicDBObject document;
	BasicDBObject detallesMovimiento;
	DBCollection coll;
	
	public  static void crearInstancia(){
		if(instancia==null)instancia=new Gestor_MongoDB();
	}
	
	public static Gestor_MongoDB getIntsancia(){
		return instancia;
	}
	
	public Gestor_MongoDB(){	
		document =new BasicDBObject();
		detallesMovimiento= new BasicDBObject();
		ConfiguracionAlgoritmoDeteccion.crearInstancia();
	}
	
	public void dbConnect(){
		MongoClient mongoClient = new MongoClient( ConfiguracionAlgoritmoDeteccion.getIntsancia().getDirDB() 
				                  , ConfiguracionAlgoritmoDeteccion.getIntsancia().getPuertoDB() );
		DB db = mongoClient.getDB( ConfiguracionAlgoritmoDeteccion.getIntsancia().getNombreDB() );
		coll = db.getCollection(ConfiguracionAlgoritmoDeteccion.getIntsancia().getColeccion());
	}
	
	public void putAtributo(String nombre, String value){
		detallesMovimiento.put(nombre, value);
	}
	
	public void putMovimiento(String numTarjeta, String value){
		document.put(numTarjeta, value);
	}
	
	public Map getStringEvent(){
		return detallesMovimiento.toMap();
	}
	
	public void insertar(String numTarjeta){
		BasicDBObject query =new BasicDBObject();
		query.put("NumTarjeta",numTarjeta);
		DBObject listItem = new BasicDBObject("Movimientos",detallesMovimiento);
		DBObject updateQuery = new BasicDBObject("$push", listItem);
		coll.update(query,updateQuery,true,true);
	}
	
	public int consultaNumMovimientos(String numTarjeta){
		int numMovimientos = 0;
		BasicDBObject queryNumber=new BasicDBObject();
		queryNumber.put("NumTarjeta",numTarjeta);

		DBCursor cursor=coll.find(queryNumber);
		List<Object> listMovimientos = new ArrayList<Object>();

		for (DBObject o : cursor) {
			listMovimientos=((List<Object>)o.toMap().get("Movimientos"));
			if(listMovimientos!=null) numMovimientos=(listMovimientos.size());
		}
		return numMovimientos;
	}
	
	public List<Object> consultaListaMovimientos(String numTarjeta){
		BasicDBObject queryNumber=new BasicDBObject();
		queryNumber.put("NumTarjeta",numTarjeta);

		DBCursor cursor=coll.find(queryNumber);
		List<Object> listMovimientos = null;

		for (DBObject o : cursor) {
			listMovimientos=((List<Object>)o.toMap().get("Movimientos"));
		}
		return listMovimientos;		
	}
	
	public void clearDocuments(){
		detallesMovimiento.clear();
		document.clear();
	}

}
