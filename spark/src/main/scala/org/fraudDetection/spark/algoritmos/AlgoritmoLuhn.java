package org.fraudDetection.spark.algoritmos;

public class AlgoritmoLuhn {
	
	public boolean testLuhn(String numTarjeta){
		int sum = 0;
		numTarjeta = numTarjeta.replaceAll(" ", "");
		boolean correcto = false;
		for(int i=0;i<numTarjeta.length();i++){
			// sacar los digitos en orden inverso     		  		
			// cada segundo número se multiplica por 2
			String digitoS = numTarjeta.substring(numTarjeta.length()-i-1, numTarjeta.length()-i);
			int digito = Integer.parseInt(digitoS);
			if(i % 2 == 1) {
				digito = digito * 2;
			}
			if(digito > 9){
				digito = digito - 9;
			}
			sum = sum + digito;
		}
		
		correcto = (sum % 10 == 0);
		return correcto;
	}
}



