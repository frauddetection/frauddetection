package org.fraudDetection.spark.algoritmos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fraudDetection.spark.dataBase.Gestor_MongoDB;
import org.fraudDetection.spark.utilidades.Utilidades;

public class DeteccionAnomalias {
	
	private HashMap<String,HashMap<String,Double>> mapAtributos;
	private HashMap<String,Double> miniMap;
	
	public DeteccionAnomalias(){
		Gestor_MongoDB.crearInstancia();
		Utilidades.crearInstancia();
		this.mapAtributos= new HashMap<String,HashMap<String,Double>>();
	}
	
	public void precalcular(String numTarjeta){

		List<Object> listMovimientos = new ArrayList<Object>();
		listMovimientos = Gestor_MongoDB.getIntsancia().consultaListaMovimientos(numTarjeta);

		for(int i=0;i<listMovimientos.size();i++){
			String movimiento=listMovimientos.get(i).toString();
			movimiento=movimiento.substring(1,movimiento.length()-1)+" ";
			String[] atributos=movimiento.split(",");
			for(int j=0;j<atributos.length;j++){
				String[] nombre_valor=atributos[j].split(":");
				String nombre=nombre_valor[0].substring(2,nombre_valor[0].length()-2);
				String valor=nombre_valor[1].substring(2,nombre_valor[1].length()-2);
				try{
					Double valorD=Double.parseDouble(valor);
					if(mapAtributos.containsKey(nombre)){
						mapAtributos.get(nombre).put("valor"+i,valorD);
					}
					else{
						HashMap<String,Double>claveValor=new HashMap<String,Double>();
						claveValor.put("valor"+i,valorD);
						mapAtributos.put(nombre,claveValor);
					}
				}
				catch(Exception e){
					if(mapAtributos.containsKey(nombre)){
						HashMap<String,Double>claveValor=new HashMap<String,Double>();
						claveValor=mapAtributos.get(nombre);
						if(claveValor.containsKey(valor)){
							claveValor.put(valor,claveValor.get(valor)+1.0);
							mapAtributos.put(nombre,claveValor);
						}
						else{
							claveValor.put(valor,1.0);
							mapAtributos.put(nombre,claveValor);
						}
					}
					else{
						HashMap<String,Double>claveValor=new HashMap<String,Double>();
						claveValor.put(valor,1.0);
						mapAtributos.put(nombre,claveValor);
					}
				}
						
			}
		}

		Double media=0.0;
		Double suma=0.0;
		Double varianza=0.0;
				
		for(String key: mapAtributos.keySet()){
			if(Utilidades.getIntsancia().existeAtributoNoCategorico(key)){
				for(String subKey: mapAtributos.get(key).keySet()){
					double cantidadAjustada=Math.log(mapAtributos.get(key).get(subKey));
					suma=suma+cantidadAjustada;
				}				
				media=suma/listMovimientos.size();							
				for(String subKey: mapAtributos.get(key).keySet()){
					double cantidadAjustada=Math.log(mapAtributos.get(key).get(subKey));
					varianza=varianza+((cantidadAjustada-media)*(cantidadAjustada-media));
				}
				varianza=varianza/listMovimientos.size();
				mapAtributos.get(key).clear();
				mapAtributos.get(key).put("Media", media);
				mapAtributos.get(key).put("Varianza",varianza);
			}
		}
	}
	
	public boolean aplicarDeteccionAnomalias(Map event,int movimientosTotales,double epsilon){
		
		miniMap = mapAtributos.get("Rama");
		double valueRama =  Utilidades.getIntsancia().ajustarValor(miniMap.get(event.get("Rama")));
		miniMap = mapAtributos.get("Lugar");
		double valueLugar = Utilidades.getIntsancia().ajustarValor(miniMap.get(event.get("Lugar")));
		miniMap = mapAtributos.get("Modo");
		double valueModo = Utilidades.getIntsancia().ajustarValor(miniMap.get(event.get("Modo")));
		miniMap = mapAtributos.get("Cantidad");
		double valueCantidad = Utilidades.getIntsancia().ajustarValor(event.get("Cantidad"));
		double valueMedia = Utilidades.getIntsancia().ajustarValor(miniMap.get("Media"));
		double valueVarianza = Utilidades.getIntsancia().ajustarValor(miniMap.get("Varianza"));
		double valueTotales = movimientosTotales;
		miniMap.clear();
		mapAtributos.clear();//En TIBCO no hacia falta, aqui se suman los atributos con el map de evento anterior
		
		double probabLugar = valueLugar/valueTotales;
		double probabRama = valueRama/valueTotales;
		double probabModo = valueModo/valueTotales;
		
		double probabCantidad = Utilidades.getIntsancia().distribucionGausiana(valueCantidad, valueMedia, valueVarianza);
		
		double probabTotal=probabLugar*probabRama*probabModo*probabCantidad;
		
		if(probabTotal<epsilon){
			return true;
		}
		else{
			return false;
		}
		
	}

}
