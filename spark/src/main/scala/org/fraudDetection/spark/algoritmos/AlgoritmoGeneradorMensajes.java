package org.fraudDetection.spark.algoritmos;

public class AlgoritmoGeneradorMensajes {
	
	public String generaMensajeComportamientoNormal(){
		
		String Nombre="Javier Mansilla";
		String Caducidad="19/18";
		String CVV="123";
		//NumTarjeta="1234123131233"; //Numero malo
		String NumTarjeta="4181583900000140";  //Numero correcto
		String Modo="Fisico";
		String Cantidad = "";
		String Rama = "";
		String Lugar = "";
		String msg = "";
		
		double randCantidad = Math.random();
		double randRama = Math.random();
		double randLugar = Math.random();

		if((randCantidad>=0)&&(randCantidad<0.25)) Cantidad="35";
		else if((randCantidad>0.25)&&(randCantidad<0.50))Cantidad="200";
		else if((randCantidad>0.50)&&(randCantidad<0.75))Cantidad="450";
		else if((randCantidad>0.75)&&(randCantidad<1))Cantidad="135";

		if((randRama>=0)&&(randRama<0.3)) Rama="Copas";
		else if((randRama>0.3)&&(randRama<0.6))Rama="Videojuegos";
		else if((randRama>0.6)&&(randRama<1))Rama="PC";

		if((randLugar>=0)&&(randLugar<0.3)) Lugar="España";
		else if((randLugar>0.3)&&(randLugar<0.6))Lugar="Francia";
		else if((randLugar>0.6)&&(randLugar<1))Lugar="Portugal";
		
		msg = "Cantidad = "+ Cantidad + ";"+
	          "Nombre = "+ Nombre + ";"+
	          "Caudicdad = "+ Caducidad + ";"+
	          "CVV = "+ CVV + ";"+
	          "NumTarjeta = "+ NumTarjeta + ";"+
	          "Modo = "+ Modo + ";"+
	          "Rama = "+ Rama + ";"+
	          "Lugar = "+ Lugar +"";
		return msg;
	}
	
	public String generaMensajeComportamientoExtraño(){
		
		String Nombre="Javier Mansilla";
		String Caducidad="19/18";
		String CVV="123";
		//NumTarjeta="1234123131233"; //Numero malo
		String NumTarjeta="4181583900000140";  //Numero correcto
		String Modo="Fisico";
		String Cantidad = "";
		String Rama = "";
		String Lugar = "";
		String msg = "";
		
		double randCantidad = Math.random();
		double randRama = Math.random();
		double randLugar = Math.random();

		if((randCantidad>=0)&&(randCantidad<0.25)) Cantidad="350";
		else if((randCantidad>0.25)&&(randCantidad<0.50))Cantidad="2000";
		else if((randCantidad>0.50)&&(randCantidad<0.75))Cantidad="850";
		else if((randCantidad>0.75)&&(randCantidad<1))Cantidad="1350";

		if((randRama>=0)&&(randRama<0.3)) Rama="Bebes";
		else if((randRama>0.3)&&(randRama<0.6))Rama="Carritos";
		else if((randRama>0.6)&&(randRama<1))Rama="Embutidos";

		if((randLugar>=0)&&(randLugar<0.3)) Lugar="Italia";
		else if((randLugar>0.3)&&(randLugar<0.6))Lugar="India";
		else if((randLugar>0.6)&&(randLugar<1))Lugar="Estados Unidos";
		
		msg = "Cantidad = "+ Cantidad + ";"+
	          "Nombre = "+ Nombre + ";"+
	          "Caudicdad = "+ Caducidad + ";"+
	          "CVV = "+ CVV + ";"+
	          "NumTarjeta = "+ NumTarjeta + ";"+
	          "Modo = "+ Modo + ";"+
	          "Rama = "+ Rama + ";"+
	          "Lugar = "+ Lugar +"";
		
		return msg;
	}	
	
}
